import 'package:flutter/material.dart';

void main() => runApp(new GettingStarted());

class GettingStarted extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return new MaterialApp(
            title: "Getting Started Title",
            home: new Scaffold(
                appBar: new AppBar(
                    title: new Center(
                        child: new Text("App Bar Title"),
                    ),
                ),
                body: new HomeWidget(),
            ),
        );
    }
}

class HomeWidget extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return new ListView.builder(
            itemCount: 20,
            itemBuilder: (context, rowNumber) {
                return new Container(
                    padding: new EdgeInsets.all(16.0),
                    child: new Column(
                        children: <Widget>[
                            new Container(
                                height: 200.0,
                                width: 400.0,
                                child: new Image.network("http://lorempixel.com/400/200"),
                            ),
                            new Container(height: 8.0,),
                            new Text("Some content of test application", style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),),
                            new Divider(color: Colors.green,),
                        ],
                    ),
                );
            },
        );
    }
}
